﻿using System;

namespace CalculatriceConsole
{
    public class Program
    {
        public static int Multiplication(int facteur_gauche, int facteur_droite)
        {
           return facteur_gauche * facteur_droite;
        }

         public static int Carre(int carre, int carre2)
        {
           return carre * carre2;
        }

         public static int AirRec(int longueur, int largeur)
        {
           return largeur * longueur;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Entrer deux chiffres à multiplier");
            int g =
            int.Parse(Console.ReadLine());
            int d = 
            int.Parse(Console.ReadLine());
            int res;
            res = Multiplication(g,d);

            Console.WriteLine("Entrer un chiffre carre ");
            int carre =
            int.Parse(Console.ReadLine());
            int carre2 = 
            int.Parse(Console.ReadLine());
            int res1;
            res1 = Carre(carre,carre2);

            Console.WriteLine("Entrer une longueur et une largeur ");
            int air1 =
            int.Parse(Console.ReadLine());
            int air2 = 
            int.Parse(Console.ReadLine());
            int res2;
            res2 = AirRec(air1,air2);
            Console.WriteLine("multiplication : {0} - Air Carre : {1} - Air Rectangle {2} ", res, res1, res2);

        }
    }
}
