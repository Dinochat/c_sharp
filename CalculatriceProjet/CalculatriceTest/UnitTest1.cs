using System;
using Xunit;

using CalculatriceConsole;

namespace CalculatriceTest
{
    public class OperationTest
    {
        [Fact]
        public void TestMultiplication()
        {
            Assert.Equal(4, Program.Multiplication(2, 2));
            Assert.Equal(21, Program.Multiplication(3, 7));
        }
                public void TestCarre()
        {
            Assert.Equal(4, Program.Multiplication(2, 2));
            Assert.Equal(9, Program.Multiplication(3, 3));
        }
                public void TestAirRec()
        {
            Assert.Equal(12, Program.Multiplication(2, 6));
        }
        
    }
}