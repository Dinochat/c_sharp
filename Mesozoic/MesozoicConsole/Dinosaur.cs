using System;

namespace MesozoicConsole
{
    public class Dinosaur
    {
        private string name;
        private string specie;
        private int age;
    
        public Dinosaur(string name, string specie, int age)
        {
            this.name = name;
            this.specie = specie;
            this.age = age;
        }
            public string getName()
        {
            return this.name;
        }
            public string setName(string newName)
        {
            this.name = newName;
            return this.name;
        }
            public string getSpecie()
        {
            return this.specie;
        }
            public string setSpecie(string newSpecie)
        {
            this.specie = newSpecie;
            return this.specie;
        }
            public int getAge()
        {
            return this.age;
            
        }
            public int setAge(int newAge)
        {
             this.age = newAge;
             return this.age;
        }
            public string sayHello()
        {
           return string.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, this.specie,this.age);
        }

        public string roar()
        {
            return "Grrr";
        }

        public string hug(Dinosaur dino) //dino = générique pour nessie car fonction donc paramètre
        {
            return string.Format("Je suis {0} et je câline {1}", this.name, dino.name);
        }
    }
}