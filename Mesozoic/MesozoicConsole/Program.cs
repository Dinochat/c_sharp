﻿using System;

namespace MesozoicConsole
{
    class Program
    {
        static void Main(string[] args)
        {   Dinosaur Louis, Blue, John; 
            Louis = new Dinosaur("Louis","Stegausaurus",12);
            Blue = new Dinosaur("Blue","velociraptor",10);
            John = new Dinosaur("John","T-Rex",20);

            Console.WriteLine("Hello {0}, {1} ", Louis.getName(), Blue.getName());  

            Console.WriteLine("{0}. {1} mange.", Blue.hug(Louis),John.getName());
        }
    }
}