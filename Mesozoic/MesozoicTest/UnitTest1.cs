using System;
using Xunit;

using MesozoicConsole;

namespace MesozoicTest
{
    public class UnitTest1
    {
        [Fact]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 13);

            Assert.Equal("Louis", louis.getName());
            Assert.Equal("Stegausaurus", louis.getSpecie());
            Assert.Equal(12, louis.getAge());

            Assert.Equal("Nessie", nessie.getName());
            Assert.Equal("Diplodocus", nessie.getSpecie());
            Assert.Equal(13, nessie.getAge());
        }

        [Fact]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Grrr", louis.roar());
        }

        [Fact]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }
        
         [Fact]
        public void TestDinosaurHug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 13);

            Assert.Equal("Je suis Louis et je câline Nessie", louis.hug(nessie)); //passe directement l'objet dinosaure

        }
    }
}
